package com.innominds.employee;

import java.util.ArrayList;
import java.util.Iterator;


public class EmployeeDB {
    ArrayList<Employee> employeeDb = new ArrayList();
    public boolean addEmployee(Employee e) {
        return employeeDb.add(e);
    }
    /**
     * Function to delete the employee
     * @param empid
     * @return
     */
    public boolean deleteEmployee(int empid) {
        boolean isRemoved = false;
        Iterator<Employee> it =employeeDb.iterator();
        while(it.hasNext()) {
            Employee emp =it.next();
            if(emp.getId()==empid) {
                isRemoved=true;
                it.remove();
            }    
        }
        return isRemoved;    
        
    }
    
    /**
     * Function to Show the pay slip of the Employee
     * @param empid
     * @return
     */
    public String showPaySlip(int empid) {
        String paySlip ="invalid employee id";
        for(Employee e:employeeDb) {
            if(e.getId()==empid) {
                paySlip="paySlip for employee id"+empid+"is"+e.getSalary();
            }
                  }
		return paySlip;
    }
    /**
     * function to return the list of employees
     * @return
     */
    public Employee[] listAll(){
        Employee[] empArray = new Employee[employeeDb.size()];
      for( int i=0;i<employeeDb.size();i++) {
          empArray[i]=employeeDb.get(i);
        }
    return empArray;
    

    }
    }