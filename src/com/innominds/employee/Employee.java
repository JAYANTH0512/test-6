package com.innominds.employee;

public class Employee {
	
	private int id;
	private String name;
	private String email;
	private char gender;
	private float salary;
	
	
	public String GetEmployeeDetails() {
		return "id=" + id + ", name=" + name + ", email=" + email + ", gender=" + gender + ", salary="
				+ salary + "]";
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public char getGender() {
		return gender;
	}


	public void setGender(char gender) {
		this.gender = gender;
	}


	public float getSalary() {
		return salary;
	}


	public void setSalary(float salary) {
		this.salary = salary;
	}


	public Employee(int id, String name, String email, char gender, float salary) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.gender = gender;
		this.salary = salary;
	}
	
}
