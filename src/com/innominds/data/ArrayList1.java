package com.innominds.data;

import java.util.ArrayList;

public  class ArrayList1 {
	ArrayList<Integer> a1 = new ArrayList();
	
	public ArrayList<Integer> saveEvenNumbers(int n){
		a1 = new ArrayList<Integer>();
		for (int i = 2; i < n; i++) {
			if(i % 2 == 0) {
				a1.add(i);
			}			
		}
		
		return a1;			
	}
	
	public void printEvenNumbers(int n) {
		ArrayList<Integer> a2 = new ArrayList();
	 for (Integer item : a1) {
		 a2.add(item * 2);
		
	}
	 System.out.println(a2);	
	}
	
	public void printEvenNumber(int n) {
		if (a1.contains(n)) {
			int index = a1.indexOf(n);
			System.out.println(a1.get(index));
			
			
		}else {
			System.out.println(0);
		}
	}
	
	public static void main(String[] args) {
		ArrayList1 q1 = new ArrayList1();
		q1.saveEvenNumbers(50);
		q1.printEvenNumbers(50);
		q1.printEvenNumber(29);
		
	}	
}